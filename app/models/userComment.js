var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserCommentShema = new Schema({
	ownerId: String,
  userId: String, // kreator konmentara
  userName: String,
  date: String,
  time: String,
  description: String
});

mongoose.model('UserComment', UserCommentShema);