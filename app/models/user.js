var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserShema = new Schema({
	name: String,
	surname: String,
  email: String,
  password: String,
  education: String,
  type: String,
  city: String,
  postNumber: String,
  address: String,
  county: String
});

mongoose.model('User', UserShema);