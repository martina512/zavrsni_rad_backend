var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AdShema = new Schema({
  userId: String,
	subject: String,
	looking: String,
  offer: String,
  category: String,
  price: Number,
  place: String,
  date: String,
  time: String,
  description: String
});

mongoose.model('Ad', AdShema);
