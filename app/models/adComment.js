var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AdCommentShema = new Schema({
	adId: String,
	userName: String,
  userId: String,
  date: String,
  time: String,
  description: String
});

mongoose.model('AdComment', AdCommentShema);