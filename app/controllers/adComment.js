var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var AdComment = mongoose.model('AdComment');
var Ad = mongoose.model('Ad');
var User = mongoose.model('User');
var Ad = mongoose.model('Ad');
var H = require('../utils/helpers');



module.exports = function (app) {
  app.use('/', router);
};

router.post('/ad/comment', H.isLoggedIn, function(req, res, next) {
  var description = req.body.description;
  var adId = req.body.adId;
  
  var adComment = new AdComment({
    adId: adId,
    userId: req.user._id,
    userName: req.user.name,
    description: description
  });

  adComment.save(function(err, newAdComment) {
    if (err) {
      res.send("Dogodila se greška kod kreiranja komentara");
      return;
    }

    res.send(newAdComment);
  });

});

router.delete('/ad/comment', H.isAdminOrSuperadmin, function(req, res, next) {
  var id = req.body.commentId;

  AdComment.findByIdAndRemove(id, function(err) {
    if (err) {
      res.send({error: 'komentar nije obrisan'});
    } else {
      res.send({status: 'Obrisano'});
    }
  });
});

router.get('/ad/comment/:adId', H.isLoggedIn, function (req, res, next) {
  var adId = req.params.adId;
  AdComment.find({adId: adId}, function(err, comments) {
    if (err) {
      res.send({error: err});
    } else {
      res.send(comments)
    };
  });
});
