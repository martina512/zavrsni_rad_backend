var express = require('express');
var mongoose = require('mongoose');
var passport = require('passport');
var router = express.Router();
var User = mongoose.model('User');
var H = require('../utils/helpers');

module.exports = function (app) {
  app.use('/', router);
};


router.post('/register', passport.authenticate('register'), function(req, res, next) {
  res.send(req.user);
});

router.post('/login', passport.authenticate('login-strategy'), function(req, res, next) {
  res.send(req.user);
});
