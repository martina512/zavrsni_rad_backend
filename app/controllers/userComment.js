var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var AdComment = mongoose.model('AdComment');
var UserComment = mongoose.model('UserComment');
var Ad = mongoose.model('Ad');
var User = mongoose.model('User');
var Ad = mongoose.model('Ad');
var H = require('../utils/helpers');



module.exports = function (app) {
  app.use('/', router);
};

router.post('/user/comment', H.isLoggedIn, function(req, res, next) {
  var description = req.body.description;
  var ownerId = req.body.ownerId;
  
  var userComment = new UserComment({
    ownerId: ownerId,
    userId: req.user._id,
    userName: req.user.name,
    description: description
  });

  userComment.save(function(err, newUserComment) {
    if (err) {
      res.send("Dogodila se greška kod kreiranja komentara");
      return;
    }

    res.send(newUserComment);
  });

});

router.delete('/user/comment', H.isAdminOrSuperadmin, function(req, res, next) {
  var id = req.body.commentId;
  console.log('brisem', id, req.body);
  UserComment.findByIdAndRemove(id, function(err) {
    if (err) {
      res.send({error: 'komentar nije obrisan'});
    } else {
      res.send({status: 'Obrisano'});
    }
  });
});

router.get('/user/comment/:ownerId', H.isLoggedIn, function (req, res, next) {
  var ownerId = req.params.ownerId;
  UserComment.find({ownerId: ownerId}, function(err, comments) {
    if (err) {
      res.send({error: err});
    } else {
      res.send(comments)
    };
  });
});

