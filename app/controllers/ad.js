var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Ad = mongoose.model('Ad');
var User = mongoose.model('User');
var H = require('../utils/helpers');

module.exports = function (app) {
  app.use('/', router);
};

router.post('/ad', H.isLoggedIn, function(req, res, next) {
  var subject = req.body.subject;
  var looking = req.body.looking;
  var offer = req.body.offer;
  var category = req.body.category;
  var price = req.body.price;
  var place = req.body.place;
  var date = req.body.date;
  var time = req.body.time;
  var description = req.body.description;

  var ad = new Ad({
    userName: req.user.name,
    userEmail: req.user.email,
    userId: req.user._id,
    subject: subject,
    looking: looking,
    offer: offer,
    category: category,
    price: price,
    place: place,
    date: date,
    time: time,
    description: description
	});
  ad.save(function(err, newAd) {
    if (err) {
      res.send({error: 'Dogodila se greška kod dohvaćanja oglasa.'});
    } else {
      res.send(newAd);
    }
	});

});


function addUserToAd(ad, adsWithUsers) {
  var promise = new Promise(function(resolve, reject) {
    User.findById(ad.userId, function(err, user) {
      if (err) {
        reject();
      } else {
        ad.user = user;
        adsWithUsers.push(ad);
        resolve();
      }
    });
  });

  return promise;
}

router.get('/ads', function (req, res, next) {
  var category = req.query.category;


  var options = {};

  if (category) {
    options.category = new RegExp(category, "i");
  }


  Ad.find(options, function(err, ads) {
    var promises = [];
    var adsWithUsers = [];

    for (var i = 0; i < ads.length; i++) {
      var ad = ads[i].toObject();

      var promise = addUserToAd(ad, adsWithUsers);

      promises.push(promise);
    }

    Promise.all(promises).then(function() {
      res.send(adsWithUsers);
    }).catch(function() {
      res.send({error: 'Dogodila se greška kod dohvaćanja oglasa.'});
    });
  });
});


router.delete('/ad', H.isAdminOrSuperadmin, function(req, res, next) {
  var id = req.body.adId;
  Ad.findByIdAndRemove(id, function(err) {
    if (err) {
      res.send({error: 'Oglas nije obrisan'});
    } else {
      res.send({status: 'Obrisano'});
    }
  });
});

router.put('/ad/:id', H.isAdminOrSuperadmin, function (req, res, next) {
  var id = req.params.id;

  Ad.findByIdAndUpdate(id, req.body, function(err, updatedAd) {
    res.send(updatedAd);
  });
});

router.get('/ad/:id', function (req, res, next) {
  var id = req.params.id;
  Ad.findById(id, function(err, ad) {
    if (err) {
      res.send({error: err});
    } else {
      User.findById(ad.userId, function(err, user) {
        if (err) {
          res.send({error: err});
        } else {
          var adJson = ad.toObject();
          adJson.user = user;
          res.send(adJson);
        }
      });
    }
  });
});

router.get('/userAds/:userId', H.isLoggedIn, function (req, res, next) {
  var userId = req.params.userId;
  Ad.find({userId: userId}, function(err, ads) {
    if (err) {
      res.send({error: err});
    } else {
      res.send(ads);
    }
  });
});
