var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = mongoose.model('User');
var H = require('../utils/helpers');

module.exports = function (app) {
  app.use('/', router);
};

router.delete('/user', H.isAdminOrSuperadmin, function(req, res, next) {
	var id = req.body.userId;
  User.findByIdAndRemove(id, function(err) {
    if (err) {
      res.send({error: 'Korisnik nije obrisan'});
    } else {
      res.send({status: 'Obrisano'});
    }
  });
});

router.get('/users', H.isAdminOrSuperadmin, function (req, res, next) {
  User.find({'type': 'korisnik'}, function(err, users) {
    res.send(users)
  });
});


router.post('/user', H.isAdminOrSuperadmin, function(req, res, next) {
  var name = req.body.name;
  var surname = req.body.surname;
  var email = req.body.email;
  var password = req.body.password;
  var education = req.body.education;
  var city = req.body.city;
  var postNumber = req.body.postNumber;
  var address = req.body.address;
  var county = req.body.county;
  
  var user = new User({
    name: name,
    surname: surname,
    email: email,
    password: password,
    education: education,
    type: 'korisnik',
    city: city,
    postNumber: postNumber,
    address: address,
    county: county
  });

  user.save(function(err, newUser) {
    if (err) {
      res.send({error: 'Dogodila se greška kod kreiranja korisnika.'});
      return;
    }

    res.send(newUser);
  });
});

router.get('/user/:id', H.isLoggedIn, function (req, res, next) {
  var id = req.params.id;
  User.findById(id, function(err, user) {
    if (err) {
      res.send({error: err});
    } else {
      res.send(user);
    }
  });
});

router.put('/user/:id', function (req, res, next) {
  var id = req.params.id;

  User.findByIdAndUpdate(id, req.body, function(err, updatedUser) {
    res.send(updatedUser);
  });

});

router.get('/logout', H.isLoggedIn, function(req, res){
  req.logout();
  res.send({korisnik: 'odjavljen'});
});

router.get('/createSuperadmin', function(req, res) {
  User.findOne({email: 'instrukcije@gmail.com'}, function(err, superadmin) {    
    if (superadmin) {
      res.send({error: 'Korisnik već postoji'});
    } else {
      var user = new User({
        email: 'instrukcije@gmail.com',
        password: '123456',
        type: 'superadmin'
      });

      user.save(function(err, newUser) {
        if (err) {
          res.send({error: 'Dogodila se greška kod kreiranja korisnika.'});
          return;
        }
        res.send(newUser);
      });
    };
  });
});
