var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = mongoose.model('User');
var H = require('../utils/helpers');

module.exports = function (app) {
  app.use('/', router);
};

router.post('/admin', H.isSuperadmin, function(req, res, next) {
	var name = req.body.name;
	var surname = req.body.surname;
 	var email = req.body.email;
  var password = req.body.password;
 	var city = req.body.city;
 	var postNumber = req.body.postNumber;
 	var address = req.body.address;
 	var county = req.body.county;
  
  var user = new User({
  	name: name,
  	surname: surname,
  	email: email,
  	password: password,
  	type: 'admin',
  	city: city,
  	postNumber: postNumber,
  	address: address,
  	county: county
  });

  user.save(function(err, newUser) {
  	if (err) {
  		res.send({error: 'Dogodila se greška kod kreiranja korisnika.'});
  		return;
  	}

  	res.send(newUser);
 	});

});

router.get('/admin', H.isSuperadmin, function (req, res, next) {
  User.find({'type': 'admin' }, function(err, users) {
  	res.send(users)
  });
});

