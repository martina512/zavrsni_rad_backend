var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = mongoose.model('User');
var Ad = mongoose.model('Ad');
var H = require('../utils/helpers');

function isAdmin(req, res, next) {
	if (req && req.user && req.user.type === 'admin') {
    next();
  } else if (req && req.user && req.user._id) {
    res.status(403);
    res.send({error: 'Nemate ovlašteni pristup'});
  } else {
    res.status(401);
    res.send({error: 'Niste prijavljeni'});
  }
}

function isSuperadmin(req, res, next) {
  if (req && req.user && req.user.type === 'superadmin') {
    next();
  } else if (req && req.user && req.user._id) {
    res.status(403);
    res.send({error: 'Nemate ovlašteni pristup'});
  } else {
    res.status(401);
    res.send({error: 'Niste prijavljeni'});
  }
}

function isLoggedIn(req, res, next) {
	// check if this is ok
	if (req && req.user && req.user._id) {
    next();
  } else {
    res.status(401);
    res.send({error: 'Niste prijavljeni'});
  }
}

function isAdminOrSuperadmin(req, res, next) {
  if (req && req.user && (req.user.type === 'admin' || req.user.type === 'superadmin' )) {
    next();
  } else if (req && req.user && req.user._id) {
    res.status(403);
    res.send({error: 'Nemate ovlašteni pristup'});
  } else {
    res.status(401);
    res.send({error: 'Niste prijavljeni'});
  }
}


module.exports = {
	isAdmin,
  isSuperadmin,
  isLoggedIn,
  isAdminOrSuperadmin
};