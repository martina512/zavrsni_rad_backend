var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'zavrsni-rad-backend'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/zavrsni-rad-backend-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'zavrsni-rad-backend'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/zavrsni-rad-backend-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'zavrsni-rad-backend'
    },
    port: process.env.PORT || 3000,
    db: 'mongodb://localhost/zavrsni-rad-backend-production'
  }
};

module.exports = config[env];
