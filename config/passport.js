var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');

module.exports = function(passport) {
	passport.serializeUser(function(user, done) {
		done(null, user);
	});

	passport.deserializeUser(function(user, done) {
		done(null, user);
	});

  passport.use('register', new LocalStrategy({
  	usernameField: 'email',
  	passwordField : 'password',
		passReqToCallback : true
  }, function(req, email, password, done) {
		User.findOne({email: email}, function(err, user) {
			if (user) {
				return done(false, null, {error: 'Email adresa već postoji'});
  		} else {
  			var user = new User({
	  	  	name: req.body.name,
	  	  	surname: req.body.surname,
			    email: email,
			    password: password,
			    education: req.body.education,
			    type: 'korisnik',
			    city: req.body.city,
			    postNumber: req.body.postNumber,
			    address: req.body.address,
			    county: req.body.county
				});

		  	user.save(function(err, newUser) {
		    	if (err) {
		      	return done({error: 'Greška kod kreiranja korisnika'}, false);
		    	}
					done(null, newUser);
		  	});
  		}
		})
	}));

	passport.use('login-strategy', new LocalStrategy({
		usernameField: 'email',
		passwordField : 'password',
		passReqToCallback : true
	}, function(req, email, password, done) {
	  User.findOne({ 'email': email }, function(err, user) {
	  	if (err) {
	  		return done(err);
	  	}

	  	if (!user) {
	  		return done(false, null, {greška: 'Korisnik ne postoji u bazi podataka'});
	  	} else {
	  		if (user.password === password) {
	  			done(false, user);
	  		} else {
	  			return done(false, null, {greška: {greška: 'Pogrešan password' }});
	  		}
	  	}
	  });
	}));
};
